package com.dogpackage;

public class Dog {
	String name;
	int nrOfPuppies;

	public static void main(String args[]) {

		Dog dog1 = new Dog();
		System.out.println(dog1.nrOfPuppies);

		Dog dog2 = new Dog();
		System.out.println(dog2.nrOfPuppies);
		System.out.println(dog2.name);

		Dog father = new Dog();
		father.name = "Max";
		father.nrOfPuppies = 1;
		System.out.println(father.name + "has" + father.nrOfPuppies + "puppies");

		Dog mother = new Dog();
		mother.name = "Bruna";
		mother.nrOfPuppies = 6;
		System.out.println(mother.name + "has" + mother.nrOfPuppies + "puppies");
		
		Dog father2 = new Dog();
		System.out.println(father2.name + "has" + father2.nrOfPuppies + "puppies");	
		
		String s1 = "1";
		String s2 = s1.concat("2");
        String s3 = s2.concat("3");
	      System.out.println(s3);	
		
        
		String string = "animals";
		System.out.println(string.indexOf('a'));
		System.out.println(string.indexOf("al"));
		System.out.println(string.indexOf('a', 4));
		System.out.println(string.indexOf("al", 5));
		 
		
		System.out.println("abcabc".replace("bc", "BC"));
		
		StringBuilder sb = new StringBuilder("start");
		sb.append("+middle");
		StringBuilder same = sb.append("end");
		System.out.println(sb);
		System.out.println(same);
		
		StringBuilder a = new StringBuilder("abc");
		StringBuilder b = a.append("de");
		b = b.append("f").append("g");
		System.out.println("a=" + a);
		System.out.println("b=" + b);
		
		
		 
	}
}
